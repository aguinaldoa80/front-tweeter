import React, { useState } from 'react';
import "./TweetBox.css";
import { Avatar, Button } from "@material-ui/core";
import db from './firebase';

function TweetBox() {
    
    const [tweetMessage, setTweetMessage] = useState("");
    const [tweetImage, setTweetImage] = useState("");
    
    const sendTweet = e => {
        e.preventDefault();
        db.collection('posts').add({
            displayName: 'Rafeh Qazi',
            username: 'cleverqazi',
            verified: true,
            text: tweetMessage,
            image: tweetImage,
            avatar: "https://img2.gratispng.com/20180330/yde/kisspng-computer-icons-avatar-businessperson-company-avatar-5abe8140104dd3.1211798015224343680668.jpg"
        });

        setTweetImage("");
        setTweetMessage("");
    }
    return <div className="tweetBox">
        <form>
            <div className="tweetBox__input">
                <Avatar src="https://img2.gratispng.com/20180330/yde/kisspng-computer-icons-avatar-businessperson-company-avatar-5abe8140104dd3.1211798015224343680668.jpg">

                </Avatar>
                <input
                 onChange={(e) => setTweetMessage(e.target.value)}
                 value={tweetMessage} 
                 placeholder="What's happening?"
                 type="text"
                />
            </div>
            <input
             className="tweetBox__imageInput" 
             placeholder="Optional: Enter image URL"
             onChange={(e) => setTweetImage(e.target.value)}
             value={tweetImage}
             type="text"
            />
            <Button onClick={sendTweet} type="submit" className="tweetBox__tweetButton">Tweet</Button>
        </form> 
    </div>;
}

export default TweetBox;
