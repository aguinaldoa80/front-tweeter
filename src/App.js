import './App.css';
import "./Sidebar";
import Sidebar from './Sidebar';
import Feed from './Feed';
import "./App.css";
import Widgets from './Widgets';

function App() {
  return (
    <div className="app">
      {/* Sidebar */}
      <Sidebar />
      {/* Feed */}
      <Feed />

      {/* Widgtes */}
      <Widgets />
    </div>
  );
}

export default App;
