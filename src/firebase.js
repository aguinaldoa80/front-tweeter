// For Firebase JS SDK v7.20.0 and later, measurementId is optional
import firebase from "firebase";

const firebaseConfig = {
    apiKey: "AIzaSyCaasZHaEpVxmN98LkDDj7_TrcBzwcCaK8",
    authDomain: "tweeter-neex.firebaseapp.com",
    projectId: "tweeter-neex",
    storageBucket: "tweeter-neex.appspot.com",
    messagingSenderId: "317128793611",
    appId: "1:317128793611:web:fe424b6a97a1af605d710e",
    measurementId: "G-WSP2WQCY70"
  };

  const firebaseApp = firebase.initializeApp(firebaseConfig);

  const db = firebaseApp.firestore();

  export default db;